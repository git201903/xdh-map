import Mask from './src/mask'

Mask.install = function (Vue) {
  Vue.component(Mask.name, Mask)
}

export default Mask
